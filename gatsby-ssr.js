/**
 * Implement Gatsby's SSR (Server Side Rendering) APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/ssr-apis/
 */

// You can delete this file if you're not using it

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _taggedTemplateLiteralLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/taggedTemplateLiteralLoose"));

var _react = _interopRequireDefault(require("react"));

var _commonTags = require("common-tags");

var generateGTMIframe = function generateGTMIframe(_ref2) {
    var id = _ref2.id;
    return (0, _commonTags.oneLine)(_templateObject2(), id);
};

function _templateObject2() {
    var data = (0, _taggedTemplateLiteralLoose2.default)(["<iframe src=\"https://www.googletagmanager.com/ns.html?id=", "", "\" height=\"0\" width=\"0\" style=\"display: none; visibility: hidden\"></iframe>"]);
    _templateObject2 = function _templateObject2() {
        return data;
    };
    return data;
}

export const onRenderBody = ({ setPreBodyComponents, setPostBodyComponents }, pluginOptions) => {
    setPreBodyComponents([_react.default.createElement("div", {
        key: 'cookie-banner',
        id: 'consent_blackbar'
    }),
    _react.default.createElement("noscript", {
        key: "plugin-google-tagmanager",
        dangerouslySetInnerHTML: {
            __html: generateGTMIframe({
                id: 'GTM-5TK7RP2'
            })
        }
    })
    ]);
    setPostBodyComponents([_react.default.createElement("script", {
        key: 'trustarc-script',
        async: 'async',
        src: '//consent.trustarc.com/notice?domain=nationwide.com&c=teconsent&js=nj&noticeType=bb&text=true&pcookie',
        crossOrigin: ''
    })
    ]);
}
export const gtmTrackingDetailDataLayer = (_event, _data, _notInit) => {
    console.log({
        'event': _event,
        'prePageviewDetails': _data
    });
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
        'event': _event,
        'prePageviewDetails': _data
    });

    if (typeof _notInit == 'undefined') {
        initGtagTrackingCode();
    }
}

export const initGtagTrackingCode = () => {
    (function (w, d, s, l, i) {
        w[l] = w[l] || []; w[l].push({
            'gtm.start':
                new Date().getTime(), event: 'gtm.js'
        }); var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l !== 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-5TK7RP2');
}
import * as React from "react"
import { StaticImage } from "gatsby-plugin-image"

import Layout from "../components/layout"
import Seo from "../components/seo"
import { gtmTrackingDetailDataLayer } from "../components/utilities";

const IndexPage = () => {
  gtmTrackingDetailDataLayer('prePageview')
  return (
    <Layout>
      <Seo title="Home" />
      <h1>Hi people</h1>
      <p>Welcome to your new Gatsby site.</p>
      <p>Now go build something great.</p>
      <StaticImage
        src="../images/gatsby-astronaut.png"
        width={300}
        quality={95}
        formats={["auto", "webp", "avif"]}
        alt="A Gatsby astronaut"
        style={{ marginBottom: `1.45rem` }}
      />
      <p>
        <a href="/page-2/">Go to page 2</a> <br />
        <a href="/using-typescript/">Go to "Using TypeScript"</a> <br />
        <a href="/using-ssr">Go to "Using SSR"</a> <br />
        <a href="/using-dsg">Go to "Using DSG"</a>
      </p>
    </Layout>
  )
}

export default IndexPage
